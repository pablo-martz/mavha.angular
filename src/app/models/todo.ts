export class Todo {
    id: number;
    description: string;
    state: TodoState;
    attachment: Attachment;
}

export class Attachment {
    id: number;
    filename: string;
}

export enum TodoState {
    Pending = 1,
    Resolved
}

export class TodoListFilter {
    searchTerm = "";
    state: TodoState = null;
    hasAttachment = null;
}

