export class GridPagination {
    page: number = 1;
    pageSize: number = 20;
    orderBy: string;
    orderDirDesc: boolean = false;
}

export class PagedResult<T>
{
    records: T[];
    page: number;
    pageSize: number;
    totalCount: number;
}