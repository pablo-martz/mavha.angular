export class PaginationState {
    page: number = 1;
    pageSize: number = 20;
    totalCount: number = 0;

    get from(): number {
        if (!this.totalCount) {
            return 0;
        }
        return (this.page - 1) * this.pageSize + 1;
    }
    
    get to(): number {
        if (!this.totalCount) {
            return 0;
        }
        let end = this.page * this.pageSize;
        return end > this.totalCount ? this.totalCount : end;
    }
}