import { Pipe, PipeTransform } from '@angular/core';
import { TodoState } from '../models/todo';

@Pipe({
  name: 'todoStateName'
})
export class TodoStateNamePipe implements PipeTransform {

  transform(todoState: TodoState) {
    return TodoState[todoState];
  }

}
