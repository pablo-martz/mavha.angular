import { Pipe, PipeTransform } from '@angular/core';
import { Attachment } from '../models/todo';

@Pipe({
  name: 'attachmentFileName'
})
export class AttachmentFileNamePipe implements PipeTransform {

  transform(value: Attachment) {
    if (!value) {
      return "(no attachment)"
    }
    return value.filename || "(?)";
  }

}
