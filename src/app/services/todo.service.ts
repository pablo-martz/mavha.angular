import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { TodoListFilter, Todo, Attachment } from '../models/todo';
import { PagedResult, GridPagination } from '../models/grid/pagination';

@Injectable({
  providedIn: 'root'
})
export class TodoService extends DataService<Todo> {
  fileUploadMaxSize = environment.fileUploadMaxSize;

  constructor(http: HttpClient) {
    super(`${environment.api.url}/todos`, http);
  }

  getPaginated(request: GetAllRequest) {
    const options: { params?: HttpParams } = {};
    if (request != null) {
      let params = new HttpParams()
        .set('searchTerm', request.filter.searchTerm)
        .set('page', request.pagination.page + "")
        .set('pageSize', request.pagination.pageSize + "")
        .set('orderBy', request.pagination.orderBy)
        .set('orderDesc', request.pagination.orderDirDesc + "");

      if (request.filter.state > 0) {
        params = params.set('state', request.filter.state + "");
      }
      if (request.filter.hasAttachment != null) {
        params = params.set('hasAttachment', request.filter.hasAttachment + "");
      }

      options.params = params;
    }

    return this.http.get<PagedResult<Todo>>(this.url, options);
  }

  uploadAttachment(file: File) {
    const formData = new FormData();
    formData.append('file', file);
    return this.http.post<Attachment>(`${environment.api.url}/attachments`, formData);
  }
}

export class GetAllRequest {
  filter = new TodoListFilter();
  pagination = new GridPagination();
}
