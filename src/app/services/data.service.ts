import { HttpClient, HttpParams } from '@angular/common/http';

export class DataService<T> {

  constructor(protected url: string, protected http: HttpClient) { }

  getAll();
  getAll(params: Object);
  getAll(params?: HttpParams | { [param: string]: string | string[] }) {
    return this.http.get(this.url, { params: params });
  }

  get(id) {
    return this.http.get<T>(`${this.url}/${id}`);
  }

  create(resource: T) {
    return this.http.post<T>(this.url, resource);
  }

  update(id, resource: T) {
    return this.http.put<T>(`${this.url}/${id}`, resource);
  }

  delete(id) {
    return this.http.delete<T>(`${this.url}/${id}`);
  }
}
