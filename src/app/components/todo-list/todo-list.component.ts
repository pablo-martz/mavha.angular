import { Component, OnInit } from '@angular/core';
import { TodoService, GetAllRequest } from 'src/app/services/todo.service';
import { TodoListFilter, Todo, TodoState, Attachment } from 'src/app/models/todo';
import { PaginationState } from 'src/app/models/grid/pagination-state';
import { GridOrder } from 'src/app/models/grid/order';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbConfirmModalComponent } from '../modals/ngb-confirm-modal/ngb-confirm-modal.component';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {

  // Grid
  todos: Todo[] = [];
  filter = new TodoListFilter();
  order: GridOrder = { orderBy: 'id', orderDirDesc: false };
  pagination = new PaginationState();
  
  // View
  selectedTodo: Todo;

  // TodoState select options
  TodoStates: { label: string, value: number }[];

  attachmentsDownloadUrl = environment.api.attachmentsDownloadUrl + '/';

  constructor(private todoService: TodoService, private modalService: NgbModal) {
    this.TodoStates = Object.keys(TodoState).filter(k => typeof TodoState[k] === "number").map(k => ({ label: k, value: TodoState[k] }));
  }

  ngOnInit(): void {
    this.getResults();
  }

  getResults() {
    // Build the request param
    let request = new GetAllRequest();
    Object.assign(request.filter, this.filter);
    request.pagination.page = this.pagination.page;
    request.pagination.pageSize = this.pagination.pageSize;
    request.pagination.orderBy = this.order.orderBy;
    request.pagination.orderDirDesc = this.order.orderDirDesc;

    this.todoService.getPaginated(request).subscribe(
      (response) => {
        // Todo records
        this.todos = response.records;
        // Pagination
        this.pagination.page = response.page;
        this.pagination.pageSize = response.pageSize;
        this.pagination.totalCount = response.totalCount;
      }, (error: HttpErrorResponse) => {
        if (error.error instanceof ErrorEvent) {
          // A client-side or network error occurred. Handle it accordingly.
          console.error('An error occurred:', error.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          console.error(`Backend returned code ${error.status}, body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError('Something bad happened; please try again later.');
      });
  }

  confirmDelete(todo: Todo) {
    // Showing a confirm dialog to delete the record
    const modalRef = this.modalService.open(NgbConfirmModalComponent);

    modalRef.componentInstance.body = `Are you sure to delete the task "${todo.description}"`;
    modalRef.componentInstance.ok = () => {
      // If confirmed, delete the record and close the dialog
      this.todoService.delete(todo.id).subscribe(() => {
        this.getResults();
      });
      modalRef.componentInstance.close();
    }
  }

  viewTodo(templateRef, todo) {
    this.selectedTodo = todo;
    this.modalService.open(templateRef).result.then(result => {
      this.selectedTodo = undefined;
    }, (reason) => {
      this.selectedTodo = undefined;
    });
  }

  onSearch() {
    this.getResults();
  }

  onPageChanged() {
    this.getResults();
  }

  onOrderChanged(orderBy) {
    if (this.order.orderBy === orderBy) {
      this.order.orderDirDesc = !this.order.orderDirDesc;
    } else {
      this.order.orderBy = orderBy;
      this.order.orderDirDesc = false;
    }
    this.getResults();
  }
}
