import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-ngb-confirm-modal',
  templateUrl: './ngb-confirm-modal.component.html',
  styleUrls: ['./ngb-confirm-modal.component.scss']
})
export class NgbConfirmModalComponent implements OnInit {
  @Input() title = "Confirm";
  @Input() body;
  @Input() useCancelBtn: boolean = true;
  buttons = [
    { text: "Ok", class: "btn btn-outline-primary", action: () => this.close('ok') },
    { text: "Cancel", class:"btn btn-outline-secondary", action: () => this.close('cancel') }
  ];
  @Input() ok = () => this.activeModal.close('ok');
  @Input() cancel = () => this.activeModal.close('cancel');
  
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
  }

  close(result?) {
    this.activeModal.close(result);
  }
}

export const NgbdConfirmModalResult = Object.freeze({
  Ok: "ok",
  Cancel: "cancel"
});