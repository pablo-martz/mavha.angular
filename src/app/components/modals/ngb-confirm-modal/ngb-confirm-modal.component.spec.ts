import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgbConfirmModalComponent } from './ngb-confirm-modal.component';

describe('NgbConfirmModalComponent', () => {
  let component: NgbConfirmModalComponent;
  let fixture: ComponentFixture<NgbConfirmModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgbConfirmModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgbConfirmModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
