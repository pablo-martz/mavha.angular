import { Component, OnInit } from '@angular/core';
import { TodoState, Todo, Attachment } from 'src/app/models/todo';
import { TodoService } from 'src/app/services/todo.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { Observable, of, throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { FileValidator } from 'src/app/form-validators/file-validator';

@Component({
  selector: 'app-todo-detail',
  templateUrl: './todo-detail.component.html',
  styleUrls: ['./todo-detail.component.scss']
})
export class TodoDetailComponent implements OnInit {
  todo = new Todo();
  form: FormGroup;
  formAttachment: Attachment;

  serviceErrors: string[] = [];

  // TodoState select options
  TodoStates: { label: string, value: number }[];

  attachmentsDownloadUrl = environment.api.attachmentsDownloadUrl + '/';

  constructor(private todoService: TodoService, private route: ActivatedRoute, private router: Router, private fb: FormBuilder) {
    this.TodoStates = Object.keys(TodoState).filter(k => typeof TodoState[k] === "number").map(k => ({ label: k, value: TodoState[k] }));
    this.createForm();
  }

  ngOnInit(): void {
    // First check
    this.route.paramMap.subscribe(params => {
      this.form.disable();

      const id = params.get('id');
      const isNew = id === 'new';

      if (!isNew && isNaN(parseInt(id))) {
        this.router.navigate(['not-found']);
      }

      let loadResource = isNew ? of(new Todo()) : this.todoService.get(id);

      loadResource.subscribe(
        resource => {
          this.todo = resource;
          this.formAttachment = this.todo.attachment;
          this.form.reset(this.todo);
          this.form.enable();
        }, (error: HttpErrorResponse) => {
          if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
          } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,

            // Not found, go away
            if (error.status == 404) {
              this.router.navigate(['not-found']);
            }
            console.error(`Backend returned code ${error.status}, body was: ${error.error}`);
          }
        });
    });
  }

  createForm() {
    this.form = this.fb.group({
      description: ['', [Validators.required, Validators.maxLength(500)]],
      state: ['', Validators.required],
      attachment: [null, FileValidator.fileMaxSize(this.todoService.fileUploadMaxSize)]
    });
  }

  onSubmit() {
    this.serviceErrors = [];

    if (this.form.invalid) {
      return;
    }

    const saveTodo = Object.assign({}, this.todo, this.form.value);

    let saveObservable: Observable<Todo>;
    if (saveTodo.id) {
      saveObservable = this.todoService.update(saveTodo.id, saveTodo);
    } else {
      saveObservable = this.todoService.create(saveTodo);
    }

    saveObservable.subscribe(() => {
      this.router.navigate(['/todos']);
    }, (error: HttpErrorResponse) => {
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        if (error.status == 400) {
          this.serviceErrors = error.error;
        } else {
          this.serviceErrors = ["There was an error saving the Task. Please, check the connectivity and try again."];
        }
        console.error(`Backend returned code ${error.status}, body was: ${error.error}`);
      }
      // return an observable with a user-facing error message
      return throwError('Something bad happened; please try again later.');
    });
  }

  deleteFormAttachment() {
    this.setFormAttachment(null);
  }

  onFileChange(event) {
    this.serviceErrors = [];
    const input = event.target as HTMLInputElement;
    const file = input.files.length ? input.files[0] : null;

    this.form.patchValue({ 'attachment': file });
    this.form.controls.attachment.markAsTouched();
    this.form.controls.attachment.markAsDirty();

    if (!file || this.form.controls.attachment.errors) {
      return;
    }

    this.todoService.uploadAttachment(file).subscribe(
      response => {
        this.setFormAttachment(response)
      }, (error: HttpErrorResponse) => {
        input.value = '';
        if (error.error instanceof ErrorEvent) {
          // A client-side or network error occurred. Handle it accordingly.
          console.error('An error occurred:', error.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong, 
          if (error.status == 400) {
            this.serviceErrors = error.error;
          } else {
            this.serviceErrors = ["There was an error uploading the file. Please, check the connectivity and try again."];
          }
          console.error(`Backend returned code ${error.status}, body was: ${error.error}`);
        }
      }
    );
  }

  onCancel() {
    this.router.navigate(['/todos']);
  }

  shouldShowErrors(control: AbstractControl) {
    return control.invalid && (control.touched/* || this.form.submitted*/);
  }

  private setFormAttachment(attachment: Attachment) {
    this.formAttachment = attachment;
    this.form.controls.attachment.patchValue(attachment);
    this.form.controls.attachment.markAsTouched();
    this.form.controls.attachment.markAsDirty();
  }
}
