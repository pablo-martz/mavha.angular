export const environment = {
  production: true,
  api: { url: "https://localhost:44369", attachmentsDownloadUrl: "https://localhost:44369/attachments" },
  fileUploadMaxSize: 26214400 //25 MB
};
